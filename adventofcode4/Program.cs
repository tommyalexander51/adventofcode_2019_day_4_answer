﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode4
{
    static class Program
    {
		static void Main(string[] args)
        {
			var input = "372304-847060";
			var lpassword = input.Split("-");
			var min = Convert.ToInt32(lpassword[0]);
			var max = Convert.ToInt32(lpassword[1]);
			var count = 0;
			for (int password = min; password <= max; password++)
			{
				if (isValidPassword(password.ToString())) {
					count++;
				}
			}
			Console.WriteLine("Total combination = " + count);
        }

		static bool isValidPassword(string password)
		{
			var sPassword = password.ToString();
			var lp = sPassword.Length;
			if (lp != 6)
			{
				return false;
			}

			var lastDigit = 0;
			var hasDouble = false;

			for (int i = 0; i < lp; i++)
			{
				var compare = Convert.ToInt32(sPassword.Substring(i, 1));
				if (compare < lastDigit)
				{
					return false;
				}

				if (compare == lastDigit)
				{
					hasDouble = true;
				}

				lastDigit = compare;
			}
			
			return hasDouble;
		}
	}
}
